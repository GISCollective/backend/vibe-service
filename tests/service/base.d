module tests.service.base;

import fluent.asserts;
import trial.discovery.spec;

import vibe.service.base;

import std.path;
import std.exception;
import std.process;
import std.file;
import std.conv;

struct TestConfig {
  string name;
}

class TestService : Service
{
  alias Configuration = TestConfig;

  static {
    bool mainThrows = false;
    bool recoverThrows = false;
    bool configureThrows = false;

    bool mainCalled;
    bool recoverCalled;
    bool configureCalled;

    bool _recover = true;

    string[] _log;
    string[] _error;
    TestConfig config;
  }

  void shutdown() nothrow {}

  /// Get the service information
  ServiceInfo info() {
    return ServiceInfo("name", "0.1.0", "description");
  }

  /// Destination for the pid file
  string pidFile() {
    return "pid.txt";
  }

  ///
  string defaultConfigFile() {
    return "./tests/config.json";
  }

  /// Log an info message
  void log(string message) {
    this._log ~= message;
  }

  /// Log an error message
  void error(string message) {
    this._error ~= message;
  }

  /// configure the service
  void configure(TestConfig config) {
    configureCalled = true;
    enforce(!configureThrows, "config error");
    this.config = config;
  }

  ///
  bool recover() {
    recoverCalled = true;
    enforce(!recoverThrows, "recover error");
    return _recover;
  }

  /// The main service logic
  int main() {
    mainCalled = true;
    enforce(!mainThrows, "main error");

    return 0;
  }
}

string val;

int command1(string[] args) {
  val = "command1";
  return 0;
}

int command2(string[] args) {
  val = "command2 " ~ args[0];
  return 1;
}

alias suite = Spec!({
  describe("Starting a service", {

    beforeEach({
      TestService.configureThrows = false;
      TestService.recoverThrows = false;
      TestService.mainThrows = false;

      TestService.configureCalled = false;
      TestService.recoverCalled = false;
      TestService.mainCalled = false;

      TestService._recover = true;
      TestService._log = [];
      TestService._error = [];
      TestService.config = TestConfig.init;

      if("pid.txt".exists) {
        "pid.txt".remove;
      }
    });

    afterEach({
      if("pid.txt".exists) {
        "pid.txt".remove;
      }
    });

    it("should create a pid file", {
      start!TestService.should.equal(0);

      "pid.txt".exists.should.equal(true);
      "pid.txt".readText.should.equal(thisProcessID.to!string);
      TestService.configureCalled.should.equal(true);
      TestService.recoverCalled.should.equal(false);
      TestService.mainCalled.should.equal(true);

      TestService.config.name.should.equal("some value");

      TestService._log.should.equal([
        "Starting `name` v0.1.0",
        "configured with `./tests/config.json`",
        "Bye!"]);
      TestService._error.should.equal([]);
    });

    it("should be able to change the default config file", {
      start!TestService(["-c", "./tests/other.json"]).should.equal(0);

      TestService.config.name.should.equal("other");

      TestService._log.should.equal([
        "Starting `name` v0.1.0",
        "configured with `./tests/other.json`",
        "Bye!"]);
      TestService._error.should.equal([]);
    });

    it("should fail if the pid exists", {
      auto pid = spawnProcess(["sleep", "1" ]);
      "pid.txt".write(pid.processID.to!string);

      start!TestService.should.equal(1);

      "pid.txt".exists.should.equal(true);
      "pid.txt".readText.should.equal(pid.processID.to!string);

      TestService.recoverCalled.should.equal(false);
      TestService.mainCalled.should.equal(false);

      TestService._log.should.equal([
        "Starting `name` v0.1.0",
        "configured with `./tests/config.json`",
        "Bye!"]);
      TestService._error.should.equal(["The server is already running"]);
    });

    it("should not fail if the pid does not exists", {
      "pid.txt".write(uint.max.to!string);

      start!TestService.should.equal(0);

      "pid.txt".exists.should.equal(true);
      "pid.txt".readText.should.equal(thisProcessID.to!string);

      TestService.recoverCalled.should.equal(true);
      TestService.mainCalled.should.equal(true);

      TestService._log.should.equal([
        "Starting `name` v0.1.0",
        "configured with `./tests/config.json`",
        "Recovering from unexpected stop",
        "Bye!"
      ]);
      TestService._error.should.equal([]);
    });

    it("should fail if the service can not recover", {
      TestService._recover = false;
      "pid.txt".write(uint.max.to!string);

      start!TestService.should.equal(1);

      "pid.txt".exists.should.equal(true);
      "pid.txt".readText.should.equal(uint.max.to!string);
      TestService.recoverCalled.should.equal(true);
      TestService.mainCalled.should.equal(false);

      TestService._log.should.equal([
        "Starting `name` v0.1.0",
        "configured with `./tests/config.json`",
        "Recovering from unexpected stop",
        "Bye!"
      ]);
      TestService._error.should.equal(["Service recovery failed"]);
    });

    describe("Logging service errors", {
      it("should log exceptions thrwon in configuration", {
        TestService.configureThrows = true;

        start!TestService.should.equal(1);

        TestService._log.should.equal([
          "Starting `name` v0.1.0",
          "configured with `./tests/config.json`",
          "Bye!"
        ]);

        TestService._error.should.equal([ "config error" ]);
      });

      it("should log exceptions thrwon in recovery", {
        "pid.txt".write(uint.max.to!string);
        TestService.recoverThrows = true;

        start!TestService.should.equal(1);

        TestService._log.should.equal([
          "Starting `name` v0.1.0",
          "configured with `./tests/config.json`",
          "Recovering from unexpected stop",
          "Bye!"
        ]);

        TestService._error.should.equal([ "recover error" ]);
      });

      it("should log exceptions thrwon in main", {
        TestService.mainThrows = true;

        start!TestService.should.equal(1);

        TestService._log.should.equal([
          "Starting `name` v0.1.0",
          "configured with `./tests/config.json`",
          "Bye!"
        ]);

        TestService._error.should.equal([ "main error" ]);
      });
    });
  });

  describe("Cleaning the service", {
    it("should remove the pid file if it exists", {
      "pid.txt".write("1");

      clean!TestService.should.equal(0);
      "pid.txt".exists.should.equal(false);
    });

    it("should be successfuly if the pid file does not exist", {
      clean!TestService.should.equal(0);
      "pid.txt".exists.should.equal(false);
    });
  });

  describe("Stopping a service", {
    afterEach({
      if("pid.txt".exists) {
        "pid.txt".remove;
      }
    });

    it("should send a SIGINT signal to the process", {
      auto pid = spawnShell("sleep 10");
      "pid.txt".write(pid.processID.to!string);

      stop!TestService.should.equal(0);

      wait(pid).should.equal(-2);
    });

    it("should do nothing the pid file does not exist", {
      stop!TestService.should.equal(0);
    });

    it("should do nothing the pid does not exist", {
      "pid.txt".write(uint.max.to!string);

      stop!TestService.should.equal(1);
    });
  });

  describe("Service commands", {
    it("should be parsed correctly", {
      ServiceCommand[string] commands;

      commands["command1"] = &command1;
      commands["command2"] = &command2;

      commands.run(["app", "command1"]);
      val.should.equal("command1");

      commands.run(["app", "command2", "arg"]).should.equal(1);
      val.should.equal("command2 arg");
    });

    it("should return an error when the command does not exist", {
      ServiceCommand[string] commands;

      commands.run(["app", "command1"]).should.equal(1);
    });

    it("should return an error when the command is not provided", {
      ServiceCommand[string] commands;

      commands.run(["app"]).should.equal(1);
    });

    it("should call the default command when it is not provided", {
      ServiceCommand[string] commands;
      commands["default"] = &command1;

      commands.run(["app"]).should.equal(0);
      val.should.equal("command1");
    });
  });

  describe("Service help", {
    it("should output the service name, version and description", {
      help!TestService.should.equal("name v0.1.0\ndescription");
    });

    it("should output the list of possible commands", {
      ServiceCommand[string] commands;

      commands["command1"] = &command1;
      commands["command2"] = &command2;

      help(commands).should.equal("command1\ncommand2");
    });

    it("should mark the default command", {
      ServiceCommand[string] commands;

      commands["command1"] = &command1;
      commands["command2"] = &command2;
      commands["default"] = commands["command1"];

      help(commands).should.equal("command1 (default)\ncommand2");
    });
  });
});
