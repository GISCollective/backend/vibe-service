module vibe.service.configuration.http;

import std.datetime;
import std.functional;
import std.conv;

import vibe.data.json;
import vibe.http.server;
import vibe.core.log;

///
struct MicroHTTP {

  ///
  static HTTPServerSettings defaultConfig() {
    auto httpSettings = new HTTPServerSettings;
    httpSettings.port = 8080;

    httpSettings.maxRequestHeaderSize = 8_192;
    httpSettings.maxRequestSize = 2_097_152;

    httpSettings.errorPageHandler = (&errorPageHandler).toDelegate;

    return httpSettings;
  }
}

///
struct HTTPConfig {
  /// Determines the server host name.
  string hostName;

  /// The port on which the HTTP server is listening.
  ushort port = 80;


  @optional:
    ///
    string[] bindAddresses;

    /// Maximum number of transferred bytes for the request header. This includes the request line the url and all headers.
    ulong maxRequestHeaderSize = 8192;

    /// Maximum number of transferred bytes per request after which the connection is closed with an error
    ulong maxRequestSize = 2_097_152;

    ///
    string serverString = "api";

    /// Interval between WebSocket ping frames.
    ulong webSocketPingInterval;

    ///
    HTTPServerSettings toVibeConfig() {
      auto httpSettings = new HTTPServerSettings;
      httpSettings.hostName = this.hostName;
      httpSettings.serverString = this.serverString;
      httpSettings.port = this.port;

      httpSettings.maxRequestHeaderSize = this.maxRequestHeaderSize;
      httpSettings.maxRequestSize = this.maxRequestSize;

      httpSettings.webSocketPingInterval = this.webSocketPingInterval.seconds;

      if(this.bindAddresses.length > 0) {
        httpSettings.bindAddresses = this.bindAddresses;
      }

      httpSettings.errorPageHandler = (&errorPageHandler).toDelegate;

      return httpSettings;
    }
}

@trusted void errorPageHandler(HTTPServerRequest request, HTTPServerResponse response, HTTPServerErrorInfo info) {
  logError("Server exception: %s %s %s", info.debugMessage, info.message, info.exception.to!string);

  Json error = Json.emptyObject;
  error["title"] = "Server exception";
  error["message"] = info.message;
  error["status"] = 500;

  Json errors = Json.emptyObject;
  errors["errors"] = Json.emptyArray;
  errors["errors"] = [ error ];

  response.writeJsonBody(errors, 500);
}
