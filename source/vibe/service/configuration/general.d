module vibe.service.configuration.general;

import std.file;
import std.path;
import std.exception;

import vibe.service.configuration.db;
import vibe.data.json;
import vibe.core.log;


/// Configurations for all services
struct GeneralConfig {
  /// The name of the service
  string serviceName = "My service";

  ///
  string serviceUrl = "http://localhost:4200";

  ///
  string apiUrl = "http://localhost:9091";

  /// Path to the folder containing db configs
  string db;

  /// Path to the folder containing the pid files
  string pidFolder;

  /// The global log level
  LogLevel logLevel = LogLevel.warn;

  @optional {
    /// Path to the folder containing the log files
    string logFolder;

    /// The stats configuration
    Stats stats;
  }

  ///
  DbConfiguration[] dbConfiguration() {
    return getDbConfiguration(this.db);
  }
}

struct Stats {
  /// How often it should create the stats
  size_t interval = 15;

  /// If it's set, the stats will be sent to the prometheus gateway
  string pushGatewayUrl;
}