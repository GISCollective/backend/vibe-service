module vibe.service.configuration.db;

import std.file;
import std.path;
import std.exception;
import std.datetime;

import vibe.service.base;

import vibe.data.json;
import vibe.db.mongo.client;
import vibe.db.mongo.mongo;
import vibe.db.mongo.settings;
import vibe.core.net;
import std.socket;
import core.thread;
import vibe.core.log;

struct DbConfiguration {
  string type;
  Json configuration;

  @optional:
    string url;
}

auto getDbConfiguration(Json configuration) {
  return getDbConfiguration(configuration["db"].to!string);
}

auto getDbConfiguration(string dbFolder) {
  enforce(dbFolder.exists, "The db configuration folder: `" ~ dbFolder ~ "` does not exist!");

  DbConfiguration configuration;

  auto mongoConfiguration = buildPath(dbFolder, "mongo.json").readText.parseJsonString;

  replaceEnvVars(mongoConfiguration);

  enforce(mongoConfiguration["type"] == "mongo", "Expected to find a mongo configuration file.");

  configuration.type = "mongo";
  configuration.configuration = mongoConfiguration["configuration"];

  if("url" in mongoConfiguration) {
    configuration.url = mongoConfiguration["url"].to!string;
  }

  return [ configuration ];
}

DbConfiguration getMongoConfig(DbConfiguration[] configurations) {
  foreach (config; configurations) {
    if(config.type == "mongo") {
      return config;
    }
  }

  throw new Exception("The mongo db config is missing");
}

MongoClient getMongoClient(DbConfiguration[] configurations) {
  auto mongoConfig = configurations.getMongoConfig;

  MongoClient client;
  string dbName = mongoConfig.configuration["database"].to!string;

  foreach(i; 0..10) {
    try {
      if(mongoConfig.url) {
        client = connectMongoDB(mongoConfig.url);
      } else {
        client = connectMongoDB(mongoConfig.toMongoSettings);
      }

      client.getDatabase(dbName).getLastError();

      return client;
    } catch (Exception e) {
      logError("Can't connect to db: %s", e.message);
      if(client !is null) {
        client.cleanupConnections;
      }

      Thread.sleep(1.seconds);
    }
  }

  logInfo("Is your mongo host up?\n%s\n\n", mongoConfig.serializeToJson.toPrettyString);
  throw new Exception("Can't connect to the DB!");
}

auto getMongoDb(DbConfiguration[] configurations) {
  auto client = configurations.getMongoClient;
  string dbName = configurations.getMongoConfig.configuration["database"].to!string;

  return client.getDatabase(dbName);
}

auto toMongoSettings(DbConfiguration configuration) {
  auto mongoSettings = configuration.configuration.deserializeJson!MongoClientSettings;

  if("appName" !in configuration.configuration) {
    configuration.configuration["appName"] = "gis-collective-unknown";
  }

  string dbName = configuration.configuration["database"].to!string;

  foreach(index, host; mongoSettings.hosts) {
    if(host.name == "localhost") {
      host.name = "127.0.0.1";
    }

    auto networkAddress = resolveHost(host.name, AddressFamily.INET, true);

    logInfo("mongo host name `%s` was resolved to `%s`.", mongoSettings.hosts[index].name, networkAddress.toAddressString);
    mongoSettings.hosts[index].name = networkAddress.toAddressString;
  }

  return mongoSettings;
}