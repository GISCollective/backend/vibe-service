module vibe.service.webservice;

import std.path;
import std.file;
import std.ascii;
import std.string;
import std.conv;
import std.datetime;
import std.algorithm;

import core.memory;
import vibe.service.base;

import vibe.core.log;
import vibe.core.core;
import vibe.core.net;
import vibe.data.json;
import vibe.inet.url;
import vibe.stream.operations;

import eventcore.core;
import vibe.service.stats;
import vibe.service.segfaultHandler;

version(Posix) {
  import core.sys.posix.signal;
  import core.sys.posix.unistd;
  import core.sys.posix.pwd;
}

private extern(C) void onSignalService(int signal) nothrow {
  logInfo("Received signal %d. Shutting down.", signal);

  logInfo("Shutting down the active service...");
  activeService.shutdown;
  exitEventLoop();
}

private struct Response { int code; string data; }
private Response httpRequest(string method)(URL url, string data) {
  Response response;
  TCPConnection connection;

  try {
    connection = connectTCP(url.host, url.port);
    connection.readTimeout(1.seconds);
  } catch(Exception e) {
    logError(e.toString);
    return response;
  }

  scope(exit) connection.close;

  while(!connection.connected) {
    try {
      yield;
    } catch(Exception e) {
      logError(e.toString);
    }
  }

  try {
    string headers = method ~ " " ~ url.pathString ~ " HTTP/1.1\r\n";
    headers ~= "Host: " ~ url.host ~ "\r\n";
    headers ~= "Connection: close\r\n";
    headers ~= "Content-Type: text/plain\r\n";

    ubyte[] rawData = data.representation.dup;
    headers ~= "Content-Length: " ~ rawData.length.to!string ~ "\r\n\r\n";

    connection.write(headers);
    connection.write(data);

    string[] responseLines = connection.readAllUTF8.split("\r\n");

    if(responseLines.length < 3) {
      return response;
    }

    string[] head = responseLines[0].split(" ");

    auto bodyStart = responseLines.countUntil("") + 1;
    response.code = head[1].to!int;
    response.data = responseLines[bodyStart..$].join("\r\n");
  } catch(Exception e) {
    logError(e.toString);
  }

  return response;
}

///
abstract class WebService(Configuration) : ConfigurableService!Configuration {
  protected {
    Configuration configuration;
    GC.ProfileStats oldProfileStats;
    URL pushGateway;
    bool shouldPushStats;
  }

  void setupSignalHandlers() {
    logTrace("setup signal handler");

    version(Posix){
      // support proper shutdown using signals
      sigset_t sigset;
      sigemptyset(&sigset);
      sigaction_t siginfo;
      siginfo.sa_handler = &onSignalService;
      siginfo.sa_mask = sigset;
      siginfo.sa_flags = SA_RESTART;
      sigaction(SIGINT, &siginfo, null);
      sigaction(SIGTERM, &siginfo, null);
    }

    version(Windows){
      // WORKAROUND: we don't care about viral @nogc attribute here!
      import std.traits;
      signal(SIGABRT, cast(ParameterTypeTuple!signal[1])&onSignalService);
      signal(SIGTERM, cast(ParameterTypeTuple!signal[1])&onSignalService);
      signal(SIGINT, cast(ParameterTypeTuple!signal[1])&onSignalService);
    }
  }

  /// Get the service information
  ServiceInfo info() {
    string version_ = import("version.txt").strip;
    string name = import("name.txt").strip;
    string description = import("description.txt").strip;

    if(version_[0] == 'v' && isDigit(version_[1])) {
      version_ = version_[1..$];
    } else {
      version_ = "0." ~ version_;
    }

    return ServiceInfo(
      name,
      version_,
      description);
  }

  /// Destination for the pid file
  string pidFile() {
    return buildPath(configuration.general.pidFolder, info.name ~ ".pid");
  }

  /// Destination for the defautl config file
  string defaultConfigFile() {
    return "config/configuration.json";
  }

  nothrow @trusted {
    /// Log an info message
    void log(string message) {
      logInfo(message);
    }

    /// Log an error message
    void error(string message) {
      logError(message);
    }

    /// Log an exception
    void error(Exception e) {
      try {
        logError(e.to!string);
      } catch(Exception ex) {
      }
    }
  }

  ///
  protected void statsTimer() nothrow @trusted {
    try {
      Stats.instance.set("EventDriverCore", eventDriver.core.waiterCount, [ "property": "waiterCount" ]);

      auto memStats = GC.stats;

      Stats.instance.set("usedMemorySize", memStats.usedSize);
      Stats.instance.set("freeMemorySize", memStats.freeSize);
      GC.minimize;

      static if(__traits(hasMember, GC, "profileStats")) {
        auto profileStats = GC.profileStats();

        Stats.instance.set("GCnumCollections", profileStats.numCollections - oldProfileStats.numCollections);
        Stats.instance.set("GCtotalCollectionTime", profileStats.totalCollectionTime.total!"usecs" - oldProfileStats.totalCollectionTime.total!"usecs");
        Stats.instance.set("GCtotalPauseTime", profileStats.totalPauseTime.total!"usecs" - oldProfileStats.totalPauseTime.total!"usecs");
        Stats.instance.set("GCmaxPauseTime", profileStats.maxPauseTime.total!"usecs" - oldProfileStats.maxPauseTime.total!"usecs");
        Stats.instance.set("GCmaxCollectionTime", profileStats.maxCollectionTime.total!"usecs" - oldProfileStats.maxCollectionTime.total!"usecs");

        oldProfileStats = profileStats;
      }

      Stats.instance.collect;
      clearDefaultStats();

      if(this.shouldPushStats) {
        try {
          auto response = httpRequest!"PUT"(pushGateway, Stats.instance.toPrometheusString(false));

          if(response.code != 200) {
            logError("Prometheus stats push error. Status: %s Message: %s", response.code, response.data);
          }
        } catch(Exception e) {
          error(e);
          this.shouldPushStats = false;
        }
      }

    } catch(Exception e) {
      error(e);
    }
  }

  /// configure the service
  void configure(Configuration configuration) {
    setupSegmentationHandler();

    Stats.instance.set("app_start", 1);

    this.configuration = configuration;

    if(configuration.general.logFolder != "") {
      auto logPath = buildPath(configuration.general.logFolder, info.name ~ ".log");
      if(logPath.exists) {
        remove(logPath);
      }
      auto logger = new FileLogger(logPath);
      logger.useColors = false;
      logger.minLevel = configuration.general.logLevel;
      logger.format = FileLogger.Format.threadTime;
      registerLogger(cast(shared)logger);
    }

    setLogLevel(configuration.general.logLevel);

    auto timer = createTimer(&this.statsTimer);
    timer.rearm(this.configuration.general.stats.interval.seconds, true);

    disableDefaultSignalHandlers();
    setupSignalHandlers();

    logInfo("this.configuration.general %s", this.configuration.general);

    if(this.configuration.general.stats.pushGatewayUrl != "") {
      auto pushGatewayUrl = this.configuration.general.stats.pushGatewayUrl;

      if(!pushGatewayUrl.endsWith("/")) {
        pushGatewayUrl ~= "/";
      }

      pushGatewayUrl ~= "job/" ~ this.configuration.http.serverString ~
        "/instance/" ~ this.configuration.http.hostName ~
        "/host/" ~ this.configuration.http.hostName;

      logInfo("Using Prometheus PUSH Gateway at: %s", pushGatewayUrl);

      this.pushGateway = URL.fromString(pushGatewayUrl);
      this.shouldPushStats = true;
    } else {
      logInfo("There is not Prometheus PUSH Gateway setup.");
    }
  }

  /// Try to recover after a crash. If returns false,
  /// the startup will be canceled
  bool recover() {
    return true;
  }
}
