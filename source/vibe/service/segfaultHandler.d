module vibe.service.segfaultHandler;

void setupSegmentationHandler()
{
  import core.runtime;

  // backtrace
  version(CRuntime_Glibc)
    import core.sys.linux.execinfo;
  else version(OSX)
    import core.sys.darwin.execinfo;
  else version(FreeBSD)
    import core.sys.freebsd.execinfo;
  else version(NetBSD)
    import core.sys.netbsd.execinfo;
  else version(Windows)
    import core.sys.windows.stacktrace;
  else version(Solaris)
    import core.sys.solaris.execinfo;

  static if( __traits( compiles, backtrace ) )
  {
    version(Posix) {
      import core.sys.posix.signal; // segv handler

      static extern (C) void segvHandler(int signum, siginfo_t* info, void* ptr ) nothrow
      {
        import core.stdc.stdio;

        core.stdc.stdio.printf("\n\n");

        if(signum == SIGSEGV) {
          core.stdc.stdio.printf("Got a Segmentation Fault! ");
        }

        if(signum == SIGBUS) {
          core.stdc.stdio.printf("Got a bus error! ");
        }

        core.stdc.stdio.printf(" This is probably a bug. Please create an issue.\n\n");

        static enum MAXFRAMES = 128;
        void*[MAXFRAMES]  callstack;
        int               numframes;

        numframes = backtrace( callstack.ptr, MAXFRAMES );
        backtrace_symbols_fd( callstack.ptr, numframes, 2);
      }

      sigaction_t action = void;
      (cast(byte*) &action)[0 .. action.sizeof] = 0;
      sigfillset( &action.sa_mask ); // block other signals
      action.sa_flags = SA_SIGINFO | SA_RESETHAND;
      action.sa_sigaction = &segvHandler;
      sigaction( SIGSEGV, &action, null );
      sigaction( SIGBUS, &action, null );
    }
  }
}