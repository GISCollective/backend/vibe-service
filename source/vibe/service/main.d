module vibe.service.main;

import vibe.service.base;
import std.file;
import std.path;
import std.stdio;
import std.traits;

ServiceCommand[string] commands;

/// print the help message
int helpService(T)(string[]) {
  string result;

  result = help!T;
  result ~= "\n\n usage: " ~ thisExePath.baseName ~ " [command]" ~ "\n\nAvailable commands:\n";
  result ~= help(commands) ~ "\n";

  writeln(result);

  return 0;
}

/// Main function for a service
int mainService(T:Service)(string[] args) {
  commands["start"] = &startClean!T;
  commands["stop"] = &stop!T;
  commands["default"] = commands["start"];
  commands["help"] = &helpService!T;

  commands["version"] = &appVersion!T;
  commands["description"] = &appDescription!T;
  commands["name"] = &appName!T;

  static foreach (name; __traits(allMembers, T)) {
    static if(hasUDA!(__traits(getMember, T, name), "action")) {
      mixin(`commands["` ~ name ~ `"] = &T.` ~ name ~ `;`);
    }
  }

  return commands.run(args);
}
