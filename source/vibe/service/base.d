module vibe.service.base;

import std.process;
import std.file;
import std.stdio : writeln, stderr;
import std.conv;
import std.array;
import std.string;
import std.algorithm;
import std.exception;
import std.process;

import vibe.data.json;

static Service activeService;

alias ServiceCommand = int function(string[]);

/// Service description
struct ServiceInfo {
  /// the service name
  string name;

  /// the service version
  string ver;

  /// the service description
  string description;
}

/// A service definition
interface Service {
  /// Get the service information
  ServiceInfo info();

  /// Destination for the pid file
  string pidFile();

  /// Log an info message
  void log(string message);

  /// Log an error message
  void error(string message);

  /// Try to recover after a crash. If returns false,
  /// the startup will be canceled. This useful for sending crash reports
  /// to your monitoring servers like sentry.
  bool recover();

  /// The main service logic
  int main();

  /// Method called when the service is shutting down
  void shutdown() nothrow;
}

/// A service definition
interface ConfigurableService(C) : Service {
  alias Configuration = C;

  /// Destination for the default config file
  string defaultConfigFile();

  /// configure the service
  void configure(Configuration configure);
}

/// Start a service
int start(T: Service)(string[] args = []) {
  auto service = new T;
  activeService = service;

  auto info = service.info;
  service.log("Starting `" ~ info.name ~ "` v" ~ info.ver);

  scope(exit) {
    service.log("Bye!");
  }

  auto configFile = service.defaultConfigFile;

  if(args.length > 0) {
    enforce(args.length == 2, "invalid arguments number");
    enforce(args[0] == "-c", "only `-c [file]` argument is permited");

    configFile = args[1];
  }

  try {
    if(!configFile.exists) {
      service.error("The conguration file `" ~ configFile ~ "` does not exist.");
      return 1;
    }

    service.log("configured with `" ~ configFile ~ "`");
    service.configure(configFile.loadJsonConfig!(T.Configuration));
  } catch (Throwable t) {
    service.error(t.msg);
    return 1;
  }

  auto pid = thisProcessID.to!string;
  auto pidFile = service.pidFile;

  if(pidFile.exists) {
    auto foundPid = pidFile.readText.to!ulong;

    if(pidExists(foundPid)) {
      service.error("The server is already running");
      return 1;
    }

    service.log("Recovering from unexpected stop");

    try {
      if(!service.recover) {
        service.error("Service recovery failed");
        return 1;
      }
    } catch (Throwable t) {
      service.error(t.msg);
      return 1;
    }
  }

  pidFile.write(thisProcessID.to!string);

  try {
    service.main();
  } catch (Throwable t) {
    service.error(t.msg);
    return 1;
  }

  return 0;
}

/// Clean a service
int clean(T:Service)(string[] args = []) {
  auto service = new T;

  auto configFile = service.defaultConfigFile;

  if(args.length > 0) {
    enforce(args.length == 2, "invalid arguments number");
    enforce(args[0] == "-c", "only `-c [file]` argument is permited");

    configFile = args[1];
  }

  try {
    service.configure(configFile.loadJsonConfig!(T.Configuration));
  } catch (Throwable t) {
    service.error(t.msg);
    return 1;
  }

  auto pidFile = service.pidFile;

  if(pidFile.exists) {
    pidFile.remove;
  }

  return 0;
}

/// starts a serice and cleans it after successfuly exit
int startClean(T:Service)(string[] args = []) {
  auto result = start!T(args);

  if(result == 0) {
    clean!T(args);
  }

  return result;
}

/// Stop a service
int stop(T:Service)(string[] args = []) {
  auto service = new T;

  auto configFile = service.defaultConfigFile;

  if(args.length > 0) {
    enforce(args.length == 2, "invalid arguments number");
    enforce(args[0] == "-c", "only `-c [file]` argument is permited");

    configFile = args[1];
  }

  try {
    service.configure(configFile.loadJsonConfig!(T.Configuration));
  } catch (Throwable t) {
    service.error(t.msg);
    return 1;
  }

  auto pidFile = service.pidFile;

  if(!pidFile.exists) {
    return 0;
  }

  auto foundPid = pidFile.readText.to!ulong;
  auto killCommand = executeShell("kill -INT " ~ foundPid.to!string);

  return killCommand.status;
}

/// Check if a pid is rrunning on the system
bool pidExists(ulong pid) {
  auto ps = executeShell("ps a");

  if(ps.status != 0) {
    return false;
  }

  return ps.output
    .split("\n")
    .map!(a => a.strip)
    .map!(a => a.splitter(' '))
    .filter!(a => !a.empty)
    .map!(a => a.front)
    .filter!(a => a.isNumeric)
    .map!(a => a.to!ulong)
    .canFind(pid);
}

/// run a command based on the cli arguments
int run(ServiceCommand[string] commands, string[] args) {
  string command = "default";
  string[] params;

  if(args.length >= 2) {
    command = args[1];
    params = args[2..$];
  }

  if(command !in commands) {
    stderr.writeln("`", command, "` command not found");
    return 1;
  }

  return commands[command](params);
}

/// Return service info
string help(T)() {
  auto service = new T;
  auto info = service.info;
  return info.name ~ " v" ~ info.ver ~ "\n" ~ info.description;
}

/// Return the app version
int appVersion(T)(string[] args = []) {
  auto service = new T;
  auto info = service.info;

  writeln(info.ver);

  return 0;
}

/// Return the app description
int appDescription(T)(string[] args = []) {
  auto service = new T;
  auto info = service.info;

  writeln(info.description);

  return 0;
}

/// Return the app name
int appName(T)(string[] args = []) {
  auto service = new T;
  auto info = service.info;

  writeln(info.name);

  return 0;
}

/// ditto
string help(ServiceCommand[string] commands) {
  string defaultName;

  if("default" in commands) {
    auto defaultCommand = commands["default"];
    defaultName = commands.byKeyValue
      .filter!(a => a.key != "default")
      .filter!(a => a.value == defaultCommand).front.key;
  }

  auto keys = commands
    .byKey
    .filter!(a => a != "default")
    .map!(a => a == defaultName ? a ~ " (default)" : a)
    .array;

  keys.sort;

  return keys.join("\n");
}

////
T loadJsonConfig(T)(string fileName) {
  auto json = readText(fileName).parseJsonString;

  replaceEnvVars(json);

  return json.deserializeJson!(T);
}

///
void replaceEnvVars(ref Json value) {
  if(value.type == Json.Type.string) {
    setStringValue(value);
    return;
  }

  if(value.type != Json.Type.object && value.type != Json.Type.array) {
    return;
  }

  if(value.type == Json.Type.object) {
    foreach(ref string k, ref Json v; value) {
      replaceEnvVars(v);
    }
  }

  if(value.type == Json.Type.array) {
    foreach(ref Json v; value) {
      replaceEnvVars(v);
    }
  }
}

string resolveEnvVar(string name) {
  return environment.get(name);
}

///
void setStringValue(ref Json value, string function(string) resolveEnvVar = &resolveEnvVar) {
  string strValue = value.to!string;

  if(strValue.length == 0) {
    return;
  }

  if(strValue[0] == '$') {
    auto envValue = resolveEnvVar(strValue[1..$]);

    enforce(envValue !is null, "The env value `" ~ strValue[1..$] ~ "` is not set.");

    value = envValue;
    return;
  }

  if(strValue[0] == '#') {
    auto envValue = resolveEnvVar(strValue[1..$]);

    enforce(envValue !is null, "The env value `" ~ strValue[1..$] ~ "` is not set.");

    value = envValue.to!long;
    return;
  }

  if(strValue.canFind("{")) {
    auto pieces = strValue.split("{").map!(a => a.split("}")).join.map!(a => Json(a)).array;
    foreach(ref val; pieces) {
      setStringValue(val, resolveEnvVar);
    }

    string finalValue = pieces.map!(a => a.to!string).join;

    value.opAssign(finalValue);
  }
}

version(unittest) {
  import fluent.asserts;

  string testReplaceValue(string name) {
    return "0" ~ name ~ "0";
  }
}

/// it should not change the string if it has no vars
unittest {
  Json value = "test";
  setStringValue(value, &testReplaceValue);
  value.should.equal("test");
}

/// it should change the string if it contains a var
unittest {
  Json value = "$test";
  setStringValue(value, &testReplaceValue);
  value.should.equal("0test0");
}

/// it should replace 2 vars
unittest {
  Json value = "{$var1}{$var2}";
  setStringValue(value, &testReplaceValue);
  value.should.equal("0var100var20");
}