module vibe.service.stats;

import std.conv;
import std.algorithm;
import std.array;
import std.string;
import std.ascii;
import std.datetime;
import vibe.http.router;

version(unittest) {
  import fluent.asserts;
}

///
struct StatValue {
  ///
  long value;

  ///
  string[string] parameters;
}

string toStrParams(const string[string] params) pure @safe {
  if(params.length == 0) {
    return "";
  }

  return "{" ~ params.byKeyValue.map!(a => normalizeKey(a.key) ~ `="` ~ normalizeValue(a.value) ~ `"`).joiner(",").array.to!string ~ "}";
}

///
class Stats {
  /// static instance
  static Stats instance;

  private {
    string[string] fieldHelp;

    StatValue[string] counters;
    StatValue[][string] gauges;

    size_t lastCollectionTime;
    StatValue[][string] collected;
  }

  ///
  static this() {
    instance = new Stats();
  }

  /// Set the value help message
  void help(string name, string message) {
    fieldHelp[name] = message;
  }

  /// Increments a gauge
  void inc(string name) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(1) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      if(stat.parameters.length == 0) {
        stat.value++;
        return;
      }
    }

    gauges[name] ~= StatValue(1);
  }

  /// ditto
  void inc(string name, string[string] params) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(1, params) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      auto params1 = stat.parameters.byKeyValue.map!(a => a.key ~ a.value).array;
      auto params2 = params.byKeyValue.map!(a => a.key ~ a.value).array;

      if(params1 == params2) {
        stat.value++;
        return;
      }
    }

    gauges[name] ~= StatValue(1, params);
  }

  /// Increments a value
  void set(string name, size_t value) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(value) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      if(stat.parameters.length == 0) {
        stat.value = value;
        return;
      }
    }

    gauges[name] ~= StatValue(value);
  }

  /// ditto
  void set(string name, size_t value, string[string] params) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(value, params) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      auto params1 = stat.parameters.byKeyValue.map!(a => a.key ~ a.value).array;
      auto params2 = params.byKeyValue.map!(a => a.key ~ a.value).array;

      if(params1 == params2) {
        stat.value = value;
        return;
      }
    }

    gauges[name] ~= StatValue(value, params);
  }

  /// Increments a value
  void avg(string name, size_t value) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(value) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      if(stat.parameters.length == 0 && stat.value != 0) {
        stat.value = (stat.value + value) / 2;
        return;
      }

      if(stat.parameters.length == 0 && stat.value == 0) {
        stat.value = value;
        return;
      }
    }

    gauges[name] ~= StatValue(value);
  }

  /// ditto
  void avg(string name, size_t value, string[string] params) @safe nothrow {
    if(name !in gauges) {
      gauges[name] = [ StatValue(value, params) ];
      return;
    }

    foreach(ref stat; gauges[name]) {
      auto params1 = stat.parameters.byKeyValue.map!(a => a.key ~ a.value).array;
      auto params2 = params.byKeyValue.map!(a => a.key ~ a.value).array;

      if(params1 == params2 && stat.value > 0) {
        stat.value = (stat.value + value) / 2;
        return;
      }

      if(params1 == params2 && stat.value == 0) {
        stat.value = value;
        return;
      }
    }

    gauges[name] ~= StatValue(value, params);
  }

  /// Prepare the stats to be logged
  void collect() {
    collected = gauges.dup;

    foreach(string key, ref list; gauges) {
      collected[key] = list.dup;
      foreach(ref item; list) {
        item.value = 0;
      }
    }

    enum posixEpochAsStd = SysTime(DateTime(1970, 1, 1, 0, 0, 0), UTC()).stdTime;

    lastCollectionTime = (Clock.currTime.toUTC.stdTime - posixEpochAsStd).convert!("hnsecs", "msecs");
  }

  /// Outputs the stats to prometheus format
  string toPrometheusString(bool withTimestamp = true) {
    immutable timestamp = lastCollectionTime.to!string;

    string result;

    foreach(ref key, ref values; collected) {
      if(result.length > 0) {
        result ~= "\n";
      }

      const normalizedKey = normalizeKey(key);

      if(key in fieldHelp) {
        result ~= "# HELP " ~ normalizedKey ~ " " ~ fieldHelp[key] ~ "\n";
      }

      result ~= "# TYPE " ~ normalizedKey ~ " gauge\n";

      foreach(ref value; values) {
        result ~= normalizedKey ~ value.parameters.toStrParams ~ " " ~ value.value.to!string;

        if(withTimestamp) {
          result ~= " " ~ timestamp;
        }

        result ~= "\n";
      }
    }

    return result;
  }
}

string normalizeKey(const ref string key) pure @trusted {
  char[] newKey;
  newKey.length = key.length;

  foreach(index, ch; key) {
    if(isAlphaNum(ch)) {
      newKey[index] = ch;
    } else {
      newKey[index] = '_';
    }
  }

  return cast(string) newKey;
}

string normalizeValue(const ref string value) pure @trusted {
  char[] newValue;
  size_t count;

  foreach(index, ch; value) {
    if(ch == '"') {
      count++;
    }
  }

  if(count == 0) {
    return value;
  }

  newValue.length = value.length + count;

  size_t pos;
  foreach(ch; value) {
    if(ch == '"') {
      newValue[pos] = '\\';
      pos++;
    }

    newValue[pos] = ch;
    pos++;
  }

  return cast(string) newValue;
}

/// Increment a value and render it
unittest {
  Stats.instance = new Stats;
  Stats.instance.help("hit_count", "Shows the number of site hits");
  Stats.instance.inc("hit_count");
  Stats.instance.inc("hit_count");

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal("# HELP hit_count Shows the number of site hits\n" ~
    "# TYPE hit_count gauge\n" ~
    "hit_count 2 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}

/// Set a value and render it
unittest {
  Stats.instance = new Stats;
  Stats.instance.set("hit_count", 64);

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal(
    "# TYPE hit_count gauge\n" ~
    "hit_count 64 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}

/// Collect all stored values
unittest {
  Stats.instance = new Stats;
  Stats.instance.inc("hit_count");
  Stats.instance.inc("other");

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal("# TYPE other gauge\n" ~
    "other 1 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n\n" ~
    "# TYPE hit_count gauge\n" ~
    "hit_count 1 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}


/// Collect values with inc parameters
unittest {
  Stats.instance = new Stats;
  Stats.instance.inc("hit_count", ["key": "value"]);
  Stats.instance.inc("hit_count", ["key": "value"]);
  Stats.instance.inc("hit_count", ["key2": "value2", "key": "value"]);
  Stats.instance.inc("hit_count", ["key2": "value2", "key": "value"]);

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal(
    "# TYPE hit_count gauge\n" ~
    "hit_count{key=\"value\"} 2 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n" ~
    "hit_count{key2=\"value2\",key=\"value\"} 2 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n" );
}


/// Escapes double quotes
unittest {
  Stats.instance = new Stats;
  Stats.instance.inc(`abc"def`, [`abc"def`: `abc"def`]);

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal(
    `# TYPE abc_def gauge` ~ "\n" ~
    `abc_def{abc_def="abc\"def"} 1 ` ~ Stats.instance.lastCollectionTime.to!string ~ "\n" );
}

/// Average stats
unittest {
  Stats.instance = new Stats;
  Stats.instance.avg("delay", 10);

  Stats.instance.collect;
  Stats.instance.toPrometheusString.should.equal(
    "# TYPE delay gauge\n" ~
    "delay 10 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");

  Stats.instance.avg("delay", 10);
  Stats.instance.avg("delay", 0);

  Stats.instance.collect;
  Stats.instance.toPrometheusString.should.equal(
    "# TYPE delay gauge\n" ~
    "delay 5 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}

/// Average stats with params
unittest {
  Stats.instance = new Stats;
  Stats.instance.avg("delay", 10, ["key": "value"]);

  Stats.instance.collect;
  Stats.instance.toPrometheusString.should.equal(
    "# TYPE delay gauge\n" ~
    `delay{key="value"} 10 ` ~ Stats.instance.lastCollectionTime.to!string ~ "\n");

  Stats.instance.avg("delay", 10, ["key": "value"]);
  Stats.instance.avg("delay", 0, ["key": "value"]);

  Stats.instance.collect;
  Stats.instance.toPrometheusString.should.equal(
    "# TYPE delay gauge\n" ~
    `delay{key="value"} 5 ` ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}

/// Collect values with set parameters
unittest {
  Stats.instance = new Stats;
  Stats.instance.set("hit_count",  1, ["key": "value"]);
  Stats.instance.set("hit_count", 10, ["key": "value"]);
  Stats.instance.set("hit_count",  2, ["key2": "value2", "key": "value"]);
  Stats.instance.set("hit_count", 20, ["key2": "value2", "key": "value"]);

  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal(
    "# TYPE hit_count gauge\n" ~
    "hit_count{key=\"value\"} 10 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n" ~
    "hit_count{key2=\"value2\",key=\"value\"} 20 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n" );
}


/// The stats should be 0 after a collection
unittest {
  Stats.instance = new Stats;
  Stats.instance.help("hit_count", "Shows the number of site hits");
  Stats.instance.inc("hit_count");

  Stats.instance.collect;
  Stats.instance.collect;

  Stats.instance.toPrometheusString.should.equal(
    "# HELP hit_count Shows the number of site hits\n" ~
    "# TYPE hit_count gauge\n" ~
    "hit_count 0 " ~ Stats.instance.lastCollectionTime.to!string ~ "\n");
}

/// Clear the stats collected by SetupStats. You should call this after each Stats.instance.collect
void clearDefaultStats() nothrow @trusted {
  Stats.instance.set("hit_count", 0);
  Stats.instance.gauges["user_agent"] = [];
  Stats.instance.gauges["hit_path"] = [];
}

///
void setupStats(URLRouter router) {
  auto last = Clock.currTime;

  Stats.instance.help("hit_count", "Shows the number of site hits");

  router.any("*", (HTTPServerRequest req, HTTPServerResponse res) {
    if(req.path != "/stats" && req.path != "/favicon.ico") {
      Stats.instance.inc("hit_count");

      string userAgent = "undefined";

      if("user-agent" in req.headers) {
        userAgent = req.headers["user-agent"];
      }

      if("User-Agent" in req.headers) {
        userAgent = req.headers["User-Agent"];
      }

      auto first = req.path.indexOf("/");

      if(first == -1) {
        first = req.path.length;
      }

      Stats.instance.inc("user_agent", ["name": userAgent]);
      Stats.instance.inc("hit_path", ["route": req.path[0..first]]);
    }
  });

  router.get("/stats", (HTTPServerRequest req, HTTPServerResponse res) {
    res.writeBody(Stats.instance.toPrometheusString, 200, "text/plain");
  });
}
